﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace example2
{
    public enum Country { Korea, China, Japan }
    class Program
    {
        static void Main(string[] args)
        {
            int number1 = 123;
            System.Int32 number2 = 123;
            Console.WriteLine("number1:{0}", number1);
            Console.WriteLine("number2:{0}", number2);

            double number3 = 123D;
            double number4 = 123;
            Console.WriteLine("number3:{0}", number3);
            Console.WriteLine("number4:{0}", number4);

            Country location1 = Country.Korea;
            Country location2 = (Country)1;
            int location3 = (int)location1;

            Console.WriteLine("location1 : {0}", location1);

            Console.WriteLine("location2 : {0}", location2);
            Console.WriteLine("location3(int) : {0}", location3);
            Console.WriteLine("location3(Country) : {0}", (Country)location3);

            int? number = null;
            int defaultNumber1 = (number == null) ? 0 : (int)number;
            Console.WriteLine("defaultNumber1 : {0}", defaultNumber1);

            int defaultNumber2 = number ?? 0;
            Console.WriteLine("defaultNumber2 : {0}", defaultNumber2);



        }
    }
}
