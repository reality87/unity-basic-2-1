﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace example3
{
    public struct Employee
    {
        public int BirthYear;
        public string Name;
    }

    class Program
    {
        public class Product
        {
            public const int ConstPrice = 1000;
            public readonly int ReadOnlyPrice;

            public Product()
            {
                this.ReadOnlyPrice = 2000;
            }

            public Product(int price)
            {
                this.ReadOnlyPrice = price;
            }
        }

        static void Main(string[] args)
        {
            Employee emp1 = new Employee();
            emp1.Name = "김도현";
            emp1.BirthYear = 1983;

            Employee emp2 = emp1;

            Console.WriteLine("emp1.BirthYear:{0}", emp1.BirthYear);
            Console.WriteLine("emp2.BirthYear:{0}", emp2.BirthYear);

            emp1.BirthYear = 1928;
            Console.WriteLine("emp1.BirthYear:{0}", emp1.BirthYear);
            Console.WriteLine("emp2.BirthYear:{0}", emp2.BirthYear);


            string value1 = "깞1";
            var value2 = "값2";
            dynamic value3 = "값3";
            Console.WriteLine("value1:{0}",value1);
            Console.WriteLine("value2:{0}",value2);
            Console.WriteLine("value3:{0}",value3);

            Console.WriteLine("ConstPrice = {0}", Product.ConstPrice);

            Product item1 = new Product();
            Console.WriteLine("new Product() : ReadOnlyPrice ={0}", item1.ReadOnlyPrice);


            Product item2 = new Product(3000);
            Console.WriteLine("new Product(3000) : ReadOnlyPrice ={0}", item2.ReadOnlyPrice);


        }
    }
}
